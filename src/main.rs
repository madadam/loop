//! Runs a given process repeatedly.

#![forbid(warnings)]
#![warn(
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    variant_size_differences
)]
#![cfg_attr(feature = "cargo-clippy", deny(clippy, clippy_pedantic))]

#[macro_use]
extern crate clap;
#[macro_use]
extern crate colour;
#[macro_use]
extern crate unwrap;

use clap::{App, AppSettings, Arg};
use std::process::{self, Command};
use std::time::{Duration, Instant};

#[cfg_attr(feature = "cargo-clippy", allow(clippy_pedantic))]
const QUIET: u64 = 1;
#[cfg_attr(feature = "cargo-clippy", allow(clippy_pedantic))]
const VERY_QUIET: u64 = 2;
#[cfg_attr(feature = "cargo-clippy", allow(clippy_pedantic))]
const SILENT: u64 = 3;

/// Validates that `count` can be parsed as a `u64`.
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn is_number(count: String) -> Result<(), String> {
    let error = format!("Can't parse {} as a positive integer.", count);
    count.parse::<u64>().map(|_| ()).map_err(|_| error)
}

/// Converts a `Duration` to a `String`
fn display_duration(duration: Duration) -> String {
    format!(
        "{}.{:06} seconds",
        duration.as_secs(),
        duration.subsec_micros()
    )
}

/// Main
#[cfg_attr(feature = "cargo-clippy", allow(use_debug))]
fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(crate_version!())
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .usage("loop [FLAGS] [OPTIONS] <PROCESS>...")
        .set_term_width(0)
        .settings(&[
            AppSettings::ArgRequiredElseHelp,
            AppSettings::TrailingVarArg,
        ]).arg(
            Arg::with_name("count")
                .short("c")
                .long("count")
                .value_name("ITERATIONS")
                .validator(is_number)
                .help(
                    "Repeats the given process <ITERATIONS> times or until the process\nreturns a \
                     non-zero value and '--allow-failures' is not used.\n<ITERATIONS> must be a \
                     positive integer.  If this option is not\nprovided, the process will be \
                     repeated indefinitely.",
                ).takes_value(true),
        ).arg(
            Arg::with_name("quiet")
                .short("q")
                .long("quiet")
                .multiple(true)
                .takes_value(false)
                .help(
                    "By default, the process's output is displayed on each iteration.  Use\nthis \
                     flag to suppress the output.  Use this flag twice (pass '-qq') to\nproduce \
                     minimal output from 'loop', or thrice (pass '-qqq') for no\noutput.  If the  \
                     process fails, the output will still be shown for that\nexecution only.",
                ),
        ).arg(
            Arg::with_name("allow-failures")
                .short("a")
                .long("allow-failures")
                .takes_value(false)
                .help("The process will be repeated even if it fails."),
        ).arg(
            Arg::with_name("process")
                .value_name("PROCESS")
                .multiple(true)
                .help("   Process to run.")
                .required(true),
        ).get_matches();

    let iterations = matches
        .value_of("count")
        .map(|count| unwrap!(count.parse::<u64>()));
    let quiet_level = matches.occurrences_of("quiet");
    let allow_failures = matches.is_present("allow-failures");

    let process: Vec<_> = matches.values_of("process").unwrap().collect();
    let mut command = Command::new(unwrap!(process.get(0)));
    let _ = command.args(unwrap!(process.get(1..)));

    let end_display_string = match iterations {
        Some(count) => count.to_string(),
        None => '\u{221E}'.to_string(),
    };
    let line_separator = std::iter::repeat('=').take(100).collect::<String>();

    if quiet_level == QUIET || quiet_level == VERY_QUIET {
        dark_cyan!("Running {} iterations of ", end_display_string);
        cyan_ln!("{:?}", process.join(" "));
    }

    let mut total_elapsed = Duration::new(0, 0);
    for current_iteration in 1.. {
        let iteration_start_time = Instant::now();
        match quiet_level {
            SILENT | VERY_QUIET => (),
            QUIET => {
                prnt!(
                    "Iteration {} of {}    ",
                    current_iteration,
                    end_display_string
                );
            }
            _ => {
                dark_cyan!(
                    "Iteration {} of {} -- Running ",
                    current_iteration,
                    end_display_string
                );
                cyan_ln!("{:?}", command);
            }
        }

        let (exit_code, output) = if quiet_level > 0 {
            match command.output() {
                Ok(output) => (
                    output.status.code().unwrap_or(-1),
                    format!(
                        "{}\n{}",
                        String::from_utf8_lossy(&output.stdout).trim(),
                        String::from_utf8_lossy(&output.stderr).trim()
                    ),
                ),
                Err(error) => (-2, format!("Failed to execute process: {}", error)),
            }
        } else {
            match command.spawn() {
                Ok(mut child) => match child.wait() {
                    Ok(exit_status) => (exit_status.code().unwrap_or(-3), String::new()),
                    Err(error) => (-4, format!("Process wasn't running: {}", error)),
                },
                Err(error) => (-5, format!("Failed to execute process: {}", error)),
            }
        };

        let iteration_elapsed = iteration_start_time.elapsed();
        total_elapsed += iteration_elapsed;
        let iteration_duration = display_duration(iteration_elapsed);
        let average_duration = display_duration(total_elapsed / current_iteration);

        if exit_code != 0 && !allow_failures {
            match quiet_level {
                VERY_QUIET => red_ln!("."),
                QUIET => red_ln!("FAILED"),
                _ => (),
            }
            if !output.is_empty() {
                prnt_ln!("{}", output);
            }
            red_ln!("FAILED on iteration {}.", current_iteration);
            process::exit(exit_code);
        }

        match quiet_level {
            VERY_QUIET => {
                if exit_code == 0 {
                    if current_iteration % 100 == 0 {
                        green_ln!(".");
                    } else {
                        green!(".");
                    }
                } else {
                    red_ln!(".");
                }
            }
            QUIET => {
                if exit_code == 0 {
                    green!("OK    ");
                } else {
                    red!("FAILED");
                }
                prnt_ln!(
                    "  This run: {}    Average: {}",
                    iteration_duration,
                    average_duration
                );
            }
            _ => (),
        }

        if exit_code != 0 {
            if !output.is_empty() {
                prnt_ln!("{}", output);
            }
            red!("FAILED on iteration {}.", current_iteration);
            if quiet_level == SILENT {
                prnt_ln!("  Continuing...");
            } else {
                prnt_ln!("");
            }
        }

        if quiet_level == 0 {
            dark_cyan_ln!(
                "Process ran for {}.  Average {}.\n",
                iteration_duration,
                average_duration
            );
            prnt_ln!("{}", line_separator);
        }

        if let Some(required_iterations) = iterations {
            if required_iterations == u64::from(current_iteration) {
                break;
            }
        }
    }
}
